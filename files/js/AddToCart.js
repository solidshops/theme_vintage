// JavaScript Document

 <!-- AJAX -->

$(document).ready(function(){
      $(".btnAddToCart").click(function(){  //detect click event on a button
      var productId = {{ product.id }}; //get id from solid template language
      var elementClicked = $(this); 
      var ajaxCallUrl = "/cart/add/" + productId + "/ajax"; //build the AJAX url
       
      //if your product has variants, send them this way:
      var data = "vartypeid_1=" + $("#vartypeid_1").val(); 
       
      $.ajax({
          url: ajaxCallUrl,
          datatype: 'json',
          data: data,
          success: function(response) {
            if(response.status == true)
            {
                if(response.items_quantity == 1)
                {
                    //update a small shopping cart if you want
                    $(".items_in_cart").text("1 item"); 
                }    
                else
                {
                    //update a small shopping cart 
                    $(".items_in_cart").text(response.items_quantity + " items"); 
                }
            }
            else
            {
                alert('Something went wrong!');
            }
          }
      });
      return(false);
    });
});
