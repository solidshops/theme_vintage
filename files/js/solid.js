/*!
 * accounting.js v0.3.1, copyright 2011 Joss Crowcroft, MIT license, http://josscrowcroft.github.com/accounting.js
 */
(function(p,z){function q(a){return!!(""===a||a&&a.charCodeAt&&a.substr)}function m(a){return u?u(a):"[object Array]"===v.call(a)}function r(a){return"[object Object]"===v.call(a)}function s(a,b){var d,a=a||{},b=b||{};for(d in b)b.hasOwnProperty(d)&&null==a[d]&&(a[d]=b[d]);return a}function j(a,b,d){var c=[],e,h;if(!a)return c;if(w&&a.map===w)return a.map(b,d);for(e=0,h=a.length;e<h;e++)c[e]=b.call(d,a[e],e,a);return c}function n(a,b){a=Math.round(Math.abs(a));return isNaN(a)?b:a}function x(a){var b=c.settings.currency.format;"function"===typeof a&&(a=a());return q(a)&&a.match("%v")?{pos:a,neg:a.replace("-","").replace("%v","-%v"),zero:a}:!a||!a.pos||!a.pos.match("%v")?!q(b)?b:c.settings.currency.format={pos:b,neg:b.replace("%v","-%v"),zero:b}:a}var c={version:"0.3.1",settings:{currency:{symbol:"$",format:"%s%v",decimal:".",thousand:",",precision:2,grouping:3},number:{precision:0,grouping:3,thousand:",",decimal:"."}}},w=Array.prototype.map,u=Array.isArray,v=Object.prototype.toString,o=c.unformat=c.parse=function(a,b){if(m(a))return j(a,function(a){return o(a,b)});a=a||0;if("number"===typeof a)return a;var b=b||".",c=RegExp("[^0-9-"+b+"]",["g"]),c=parseFloat((""+a).replace(/\((.*)\)/,"-$1").replace(c,"").replace(b,"."));return!isNaN(c)?c:0},y=c.toFixed=function(a,b){var b=n(b,c.settings.number.precision),d=Math.pow(10,b);return(Math.round(c.unformat(a)*d)/d).toFixed(b)},t=c.formatNumber=function(a,b,d,i){if(m(a))return j(a,function(a){return t(a,b,d,i)});var a=o(a),e=s(r(b)?b:{precision:b,thousand:d,decimal:i},c.settings.number),h=n(e.precision),f=0>a?"-":"",g=parseInt(y(Math.abs(a||0),h),10)+"",l=3<g.length?g.length%3:0;return f+(l?g.substr(0,l)+e.thousand:"")+g.substr(l).replace(/(\d{3})(?=\d)/g,"$1"+e.thousand)+(h?e.decimal+y(Math.abs(a),h).split(".")[1]:"")},A=c.formatMoney=function(a,b,d,i,e,h){if(m(a))return j(a,function(a){return A(a,b,d,i,e,h)});var a=o(a),f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format);return(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal))};c.formatColumn=function(a,b,d,i,e,h){if(!a)return[];var f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format),l=g.pos.indexOf("%s")<g.pos.indexOf("%v")?!0:!1,k=0,a=j(a,function(a){if(m(a))return c.formatColumn(a,f);a=o(a);a=(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal));if(a.length>k)k=a.length;return a});return j(a,function(a){return q(a)&&a.length<k?l?a.replace(f.symbol,f.symbol+Array(k-a.length+1).join(" ")):Array(k-a.length+1).join(" ")+a:a})};if("undefined"!==typeof exports){if("undefined"!==typeof module&&module.exports)exports=module.exports=c;exports.accounting=c}else"function"===typeof define&&define.amd?define([],function(){return c}):(c.noConflict=function(a){return function(){p.accounting=a;c.noConflict=z;return c}}(p.accounting),p.accounting=c)})(this);

var SolidJS = function() {
	  var opt = new Array();
      return {
   	  
        enhanceVariants : function(product, p_options) {
              var variant_dropdowns = new Array();
              opt[product.id] = new Array();
              opt[product.id]['options'] = p_options;
              if(opt[product.id]['options']['placeholder_variants'] == undefined)
              {
           	   	  //set default value
            	  opt[product.id]['options']['placeholder_variants'] = "placeholder_variants";
              }
              
              document.getElementById(opt[product.id]['options']['placeholder_variants']).innerHTML = "";
              for(var i=0; i<product.variants.length; i++)
              {
                var newLabel = document.createElement("label");
                newLabel.innerHTML = product.variants[i].name;
                newLabel.setAttribute("for", "variant_" + product.variants[i].id + "_prod" + product.id);//make this unique in case of multiple products on one page
                
                var newSelect = document.createElement("select");
                newSelect.setAttribute("id", "variant_" + product.variants[i].id + "_prod" + product.id);//make this unique in case of multiple products on one page
                newSelect.setAttribute("name", "variant_" + product.variants[i].id);                

                variant_dropdowns.push("variant_"+product.variants[i].id + "_prod" + product.id );
                
                //START BUGFIX IE8 replacement
                if(opt[product.id]['options']['option_text'] != undefined)
                {
                	var myOption =  new Option(opt[product.id]['options']['option_text'], '');
                }else{
                	 var myOption =  new Option('-- Select one --', '-');
                }
                newSelect.add( myOption, null);
                newSelect.onchange = updatePriceAndStock;
                
                for(var j=0; j<product.variants[i].options.length; j++)
                {
                	var myOption =  new Option(product.variants[i].options[j].name,product.variants[i].options[j].id);
                    newSelect.add(myOption, null);
                }
                //END BUGFIX IE8 replacement

                document.getElementById(opt[product.id]['options']['placeholder_variants']).appendChild(newLabel);
                document.getElementById(opt[product.id]['options']['placeholder_variants']).appendChild(newSelect);
              }
              
              function sortNumber(a,b)
              {
                  return a - b;
              }

              /*
               * FUNCTION THAT UPDATES THE PRICE INSIDE A STORE PAGE
               * WITHOUT USING A CUSTOM CALLBACK FUNCTION	 
               */
				function updatePriceInPage()
				{
					//user has not defined a custom callback function, so let us handle price changes
				    //define where to store price
				    if(opt[product.id]['options']['placeholder_price'] == undefined)
				    {
				 	   //set default value
				 	   opt[product.id]['options']['placeholder_price'] = "placeholder_price";
				    }
				    var element_placeholder_price = document.getElementById(opt[product.id]['options']['placeholder_price']);
				    if(element_placeholder_price != null)
				    {
				 	   //update the price field with the correct value
				        element_placeholder_price.innerHTML =  accounting.formatMoney(product.variant_combinations[i].price, SolidJS.symbol, SolidJS.precision, SolidJS.thousand , SolidJS.decimal);
				    }	
				}
              
              //this function updates the price in 'placeholder_price' based on the selection of a variant combination
              function updatePriceAndStock()
              { 
            	  var variantCombination = new Array();
                  for(i=0; i<variant_dropdowns.length; i++)
                  {
                      var selectChanged = document.getElementById(variant_dropdowns[i]);
                      if(!isNaN(selectChanged.value))
                          variantCombination.push(parseInt(selectChanged.value));
                      else
                      {
                    	  //set stock and pricing info back to default
                          return(false);
                      }
                  }
                  
                                   
                variantCombination.sort(sortNumber);
                variantCombination = variantCombination.join("_");
                
                //find the correct price for the selected variant combinations
                for(i=0; i<product.variant_combinations.length; i++)
                {
                     if(product.variant_combinations[i].id == variantCombination)
                     {
                       //update price
                    	 
                       //if user has defined a callback function, we let him handle the rest
                       if(opt[product.id]['options']['callback_price'] != undefined)
                       {
                    	   var function_name = opt[product.id]['options']['callback_price'];
                    	   if (typeof(window[function_name]) === "function")
                    	   {
                    		   var result = {"id":product.id,"price":product.variant_combinations[i].price};
                    		   window[function_name](result);
                    	   }
                       }
                       else
                       {  
                    	   updatePriceInPage();
                       }
                       
                       
                       //notify if out of stock
                       if(product.track_stock == 0 || product.always_available == true) return(false); //if stock is not available, it's set on product level
                       
                       //get current stock level
                       var current_stock;
                       if(product.track_stock == 1)
                       {
                    	   current_stock = product.stock; //track stock on product level
                       }
                       else if(product.track_stock == 2)
                       {
                    	   current_stock = product.variant_combinations[i].stock; //track stock on variant combination level
                       }
                            
                       
                       //if user has defined a callback function, we let him handle the rest
                       if(opt[product.id]['options']['callback_stock'] != undefined)
                       {
                    	   var function_name = opt[product.id]['options']['callback_stock'];
                    	   if (typeof(window[function_name]) === "function")
                    	   {
                    		   var result = {"id":product.id,"stock":current_stock};
                    		   window[function_name](result);
                    	   }
                       }
                       else
                       {
	                       if(SolidJS.placeholder_stock == undefined)
	                       {
	                    	   //set default value
	                    	   SolidJS.placeholder_stock = "placeholder_stock";
	                       }
	                       
	                       var element_placeholder_stock = document.getElementById(SolidJS.placeholder_stock);
	                       if(element_placeholder_stock != null)
	                       {
	                          if(current_stock < 1)
	                          {
	                              element_placeholder_stock.innerHTML = "Out of stock";
	                          }
	                          else
	                          {
	                              element_placeholder_stock.innerHTML = current_stock + " in stock";
	                          }
	                       }
                       }
                       break;
                     }
                }
              }
         
        },
        FormatCurrency : function(p_price) {
          // convert currency into currect format
        	return(accounting.formatMoney(p_price, SolidJS.symbol, SolidJS.precision, SolidJS.thousand , SolidJS.decimal));
        }
      };
    }();